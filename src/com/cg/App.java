package com.cg;

import java.util.Arrays;
import java.util.List;

public class App {
	private static final String TOO_LOW = "too low";
	private static final String TOO_HIGH = "too high";
	private static final String RIGHT_ON = "right on";

	public static void main(String[] args) {
		List<String> list = Arrays.asList("27 too high", "56 too high", "32 too low");

//		List<String> list = Arrays.asList("50 too low", "55 too low", "45 too low", "44 too low", "62 too high",
//				"80 too high", "51 too low","61 right on");
		guessncheating(list);
	}

	static void guessncheating(List<String> list) {
		int min = 0, max = 100, round = 0;
		for (String str : list) {
			String a[] = str.split(" ");
			int number = Integer.parseInt(a[0]);
			String desc = a[1] + " " + a[2];
			if (desc.equals(TOO_LOW)) {
				if (min < number) {
					min = number;
				}
				round++;
			} else if (desc.equals(TOO_HIGH)) {
				if (max > number) {
					max = number;
				}
				round++;
			} else if (desc.equals(RIGHT_ON)) {
				if (number >= min && number <= max) {
					System.out.println("No evidence of cheating");
					return;
				} else {
					round++;
					System.out.println("Alice cheated in round " + round);
					return;
				}
			}
			if (max <= min) {
				System.out.println("Alice cheated in round " + round);
				return;
			} else if (max - min == 1) {
				System.out.println("Alice cheated in round " + round);
				return;
			}
		}
	}
}
